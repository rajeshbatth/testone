Ext.define('TestOne.store.AppStore',{
    extend:'Ext.data.Store',
    config:{
        autoLoad:true,
        model:'TestOne.model.AppModel',
        proxy: {
            type: 'jsonp',
            url: 'http://192.168.1.2/temp/jsonp.php',
            reader:{
                type:'json',
                rootProperty: 'objects'
            }
        }
    }
});