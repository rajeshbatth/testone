Ext.define('TestOne.store.UserStore',{
    extend:'Ext.data.Store',
    config:{
        model:'TestOne.model.UserModel',
        storeId:'userstore',
        proxy: {
            type: 'rest',
            url: 'http://onitsown.com/services/api/v1/user/?format=json',
            reader:{
                type:'json',
                rootProperty: 'objects'
            }
        }
    }
});