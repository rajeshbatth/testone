Ext.define('TestOne.model.UserModel',{
    extend:'Ext.data.Model',
    config:{
        fields: [
            { name: 'name', type: 'string' },
            { name: 'email', type: 'string' }
        ]

    }
});