Ext.define('TestOne.model.AppModel',{
    extend:'Ext.data.Model',
    config:{
        fields: [
            { name: 'name', type: 'string' },
            { name: 'pkg', type: 'string' }
        ]

    }
});