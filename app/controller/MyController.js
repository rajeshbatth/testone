Ext.define('TestOne.controller.MyController', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
        },
        control: {
            'button[action=signupsubmit]':{
                tap:'onSignUpSubmit'
            }
        }
    },

    onSignUpSubmit: function (v) {
        var store = Ext.getStore('userstore');
        var formValues = v.up('formpanel').getValues();
        var user = Ext.create('TestOne.model.UserModel',formValues);
        store.add(user);
        store.sync();
        console.log("Success!!");
        console.debug(store.getAt(0));
    }
});