Ext.define('TestOne.view.AppView', {
    extend: 'Ext.navigation.View',
    xtype: 'appsView',
    config: {
        layout: 'card',
        title: 'Apps',
        items: [
            {
                title: 'Apps',
                xtype: 'list',
                store: 'AppStore',
                itemTpl: '{name}',
                listeners: {
                    select: function (view, record) {
                        console.debug(view.up('appsView'));
                        view.up('appsView').push({
                            title: record.get('name'),
                            html: 'Some content'
                        });
                    }
                }
            }
        ]
    }
});