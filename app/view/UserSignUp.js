Ext.define('TestOne.view.UserSignUp', {
    extend: 'Ext.form.FormPanel',
    xtype: 'userSignUp',
    requires: [
        'Ext.TitleBar',
        'Ext.form.FieldSet',
        'Ext.field.Email'
    ],
    config: {
        items: [

            {
                xtype: 'fieldset',
                title: 'Enter your personel details',
                items: [
                    {
                        xtype: 'textfield',
                        name: 'name',
                        label: 'Name'
                    },
                    {
                        xtype: 'emailfield',
                        name: 'email',
                        label: 'Email'
                    },
                    {
                        xtype: 'passwordfield',
                        name: 'password',
                        label: 'Password'
                    },
                    {
                        xtype: 'button',
                        text: 'Submit',
                        action:'signupsubmit'
                    }

                ]
            }
        ]

    }
});