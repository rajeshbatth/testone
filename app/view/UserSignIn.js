Ext.define('TestOne.view.UserSignIn', {
    extend: 'Ext.form.FormPanel',
    xtype: 'userSignIn',

    requires: [
        'Ext.TitleBar',
        'Ext.form.FieldSet',
        'Ext.field.Email'
    ],
    config: {
        items: [
            {
                xtype: 'fieldset',
                title: 'Please login to continue',
                items: [
                    {
                        xtype: 'emailfield',
                        name: 'email',
                        label: 'Email'
                    },
                    {
                        xtype: 'passwordfield',
                        name: 'password',
                        label: 'Password'
                    }
                ]
            }
        ]
    }
});