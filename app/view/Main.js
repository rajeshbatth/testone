Ext.define('TestOne.view.Main', {
    extend: 'Ext.navigation.View',
    xtype: 'main',

    config: {
        navigationBar: {
            items:[
                {
                    xtype:'button',
                    text:'Sign up',
                    align:'right',
                    handler:function(){
                        this.up('navigationview').push({xtype:'userSignUp'});
                    }
                }
            ]
        },
        items: [
            {
                xtype: 'userSignIn',
                title: 'Sign in'
            }
        ]
    }
});
